using System;
using WebMVC.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebMVC.ContactController
{

    public class ContactController : Controller
    {
        private static List<ContactViewModel> contactView = new List<ContactViewModel>(){
            new ContactViewModel(1, "Susan Wojcicki", "susan@youtube,com", "Jakarta"),
            new ContactViewModel(2, "Susan Wojcicki", "susan@youtube,com", "Jakarta"),
            new ContactViewModel(3, "Susan Wojcicki", "susan@youtube,com", "Jakarta"),
            new ContactViewModel(4, "Susan Wojcicki", "susan@youtube,com", "Jakarta")
        };

        public IActionResult Index()
        {
            return View(contactView);
        }

    }

}