using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;
using WebMVC.Models;
using System.Threading.Tasks;


namespace WebMVC.ProductController
{
    public class ProductController : Controller
    {

        private static List<ProductViewModel> _productViewModels = new List<ProductViewModel>(){

            new ProductViewModel(1, "Jus Durian", "Minuman", 10000),
            new ProductViewModel(2, "Jus Mangga", "Minuman", 8000),
            new ProductViewModel(3, "Jus Semangka", "Minuman", 10000),

        };

        public IActionResult Index()
        {

            return View(_productViewModels);


        }

        public IActionResult AddProduct()
        {
            // Menampilkan halamaan tambah product
            return View();
        }

        [HttpPost]
        public IActionResult Save(int id, [Bind("Id, Name, Category, Price")] ProductViewModel product)
        {

            _productViewModels.Add(product);
            return Redirect("Index");

        }

        // Update Product
        public IActionResult EditProduct(int? id)
        {
            // Menampilkan halaman edit product
            ProductViewModel product = _productViewModels.Find(x => x.Id.Equals(id));
            return View(product);
        }

        // Proses Update product

        public IActionResult UpdateProduct(int id, [Bind("Id, Name, Category, Price")] ProductViewModel product)
        {

            // Pertama menghapus data lama
            ProductViewModel productOld = _productViewModels.Find(x => x.Id.Equals(id));
            _productViewModels.Remove(productOld);

            // Menambahkan data baru
            _productViewModels.Add(product);
            return Redirect("Index");

        }


        public IActionResult Delete(int? id)
        {

            // find data yang akan dihapus
            ProductViewModel product = _productViewModels.Find(x => x.Id.Equals(id));


            // Delete product 
            _productViewModels.Remove(product);
            return View(product);

        }


        public IActionResult Details(int? id)
        {

            // find menggunakan Linq
            ProductViewModel product = (from p in _productViewModels
                                        where p.Id.Equals(id)
                                        select p).SingleOrDefault(new ProductViewModel());

            return View(product);

        }


    }
}