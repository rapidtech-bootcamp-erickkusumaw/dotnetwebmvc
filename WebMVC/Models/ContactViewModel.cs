namespace WebMVC.Models
{

    public class ContactViewModel
    {

        public int Id { get; set; }

        public String Name { get; set; }

        public String Email { get; set; }

        public String Address { get; set; }


        public ContactViewModel()
        {

        }

        public ContactViewModel(int id, String name, String email, String address)
        {

            Id = id;
            Name = name;
            Email = email;
            Address = address;
        }

    }
}